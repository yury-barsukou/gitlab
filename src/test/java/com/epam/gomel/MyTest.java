package com.epam.gomel;

import org.junit.Assert;
import org.junit.Test;

public class MyTest {

  @Test
  public void sumTest() {
    Assert.assertEquals("Result of the Sum operation is wrong!", 2 + 2, 5);
  }

  @Test
  public void multTest() {
    Assert.assertEquals("Result of the Mult operation is wrong!", 2 * 3, 6);
  }
}
